package com.sfg.springframeworkguruproject2;

import com.sfg.springframeworkguruproject2.controllers.*;
import com.sfg.springframeworkguruproject2.examplebeans.FakeDataSource;
import com.sfg.springframeworkguruproject2.examplebeans.FakeJmsBroker;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class SpringframeworkguruProject2Application {

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(SpringframeworkguruProject2Application.class, args);

        MyController myController = (MyController) ctx.getBean("myController");

        FakeDataSource fakeDataSource = (FakeDataSource) ctx.getBean(FakeDataSource.class);

        System.out.print("Username: ");
        System.out.println(fakeDataSource.getUser());

        System.out.print("Password: ");
        System.out.println(fakeDataSource.getPassword());

        System.out.print("DB URL: ");
        System.out.println(fakeDataSource.getUrl());

        FakeJmsBroker fakeJmsBroker = (FakeJmsBroker) ctx.getBean(FakeJmsBroker.class);

        System.out.print("Jms Username: ");
        System.out.println(fakeJmsBroker.getUsername());

        System.out.print("Jms Password: ");
        System.out.println(fakeJmsBroker.getPassword());

        System.out.print("Jms DB URL: ");
        System.out.println(fakeJmsBroker.getUrl());
    }

}

package com.sfg.springframeworkguruproject2.services;

public interface GreetingService {

    String sayGreeting();
}

package com.sfg.springframeworkguruproject2.services;

public interface GreetingRepository {

    String getEnglishGreeting();
    String getSpanishGreeting();
    String getGermanGreeting();
}
